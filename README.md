#### This project is for the Devops Bootcamp Exercise for 
#### "Container Orchestration with Kubernetes" 

**Demo Project:**

- Deploy MSQL Database using helm with 2 replicas
- Deploy a Java web application in K8s cluster from private Docker registry
- Deploy phpmyadmin for UI access to the MSQL Database
- Deploy Ingress Controller via Helm to allow access to Java web app via browser


**Technologies used:**

Kubernetes cluster via Digital Ocean, Helm, Dockerhub


**Project Description:**

- Created Secret for credentials for the private Docker
- Configured the Docker registry secret in application
- Created ConfigMap and Secret config files to setup connection between Mysql DB and Java web app
- Created Deployment component for Java web app
- Deployed web application image from our private Docker registry in K8s cluster
- Created Helm Chart to automate deployment of Java web application (in repo https://gitlab.com/jeffrey6833536/k8-exercise/helm-chart-4-javamysql-app.git).





